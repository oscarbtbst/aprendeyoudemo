@CONST_TITLE_APP = 'Aprende YouTube - Todos tus cursos de YouTube donde quiera que estés'
@CONST_DESCRIPTION_APP = 'Aplicación que permite sincronizar todos tus cursos de YouTube en la nube.'

#Session constants
@USER_MENU_KEY = 'userMenuOpen'
@SHOW_CONNECTION_ISSUE_KEY = 'showConnectionIssue'
@USER_MODE_READ = 'userModeRead'
@USER_MODE_READ_LISTS_SHOW = 'useModeReadListsShow'
@MENU_KEY = 'menuOpen'
@CHANGING_USER_MODE = 'changingUserMode'

#Integer constants
@CONNECTION_ISSUE_TIMEOUT = 5000

#String constants
@CONST_THEME = 'Tema'
@CONST_SIGN_IN = 'Entrada'
@CONST_JOIN = 'Registro'
@CONST_LOGOUT = 'Salir'
@CONST_TRYING_CONNECT = 'Intentando conectar'
@CONST_SEEMS_CONNECTION_ISSUE = 'Parece que hay un problema de conexión'
@CONST_NEW_TOPIC = 'Nuevo tema'
@CONST_CHANGE_TO_EDIT_MODE = 'Cambio a modo edición'
@CONST_CHANGE_TO_READ_MODE = 'Cambio a modo lectura'
@CONST_MOVE_TOPIC_DOWN = 'Cambiar posición tema'

@CONST_SELECT_AN_ACTION = 'Seleccionar una acción'
@CONST_MAKE_TOPIC_PUBLIC = 'Hacer el tema público'
@CONST_MAKE_TOPIC_PRIVATE = 'Hacer el tema privado'
@CONST_SHARE_TOPIC_REGISTERED_USERS = 'Compartir el tema con usuarios registrados'
@CONST_DONT_SHARE_TOPIC_REGISTERED_USERS = 'NO compartir el tema con usuarios registrados'
@CONST_DELETE = 'Eliminar'
@CONST_CANCEL = 'Cancelar'
@CONST_SHOW_MENU = 'Mostrar menu'
@CONST_THERE_IS_NO_ELEMENTS_HERE = 'No hay elementos aquí'
@CONST_ADD_NEW_ELEMENTS_WITH_FIELD_ABOVE = 'Añade nuevos elementos mediante el campo de la barra'
@CONST_LOADING_ELEMENTS = 'Cargando elementos...'
@CONST_WRITE_NEW_ELEMENT_HERE = 'Escribe un nuevo elemento aquí'
@CONST_ELEMENT_CONTENT = 'Contenido del elemento'

@CONST_SING_IN_ALLOWS_MANAGE_PRIVATE_TOPICS = 'Entrar permite gestionar temas privados'
@CONST_SIGN_IN_DOT = 'Entrada.'
@CONST_NEED_ACOUNT_JOIN_NOW = 'Necesita una cuenta? Regístrese ahora.'

@CONST_JOIN_DOT = 'Registro.'
@CONST_JOIN_ALLOWS_MANAGE_PRIVATE_TOPICS = 'Registrarse permite gestionar temas privados'
@CONST_JOIN_NOW = 'Regístrese ahora'
@CONST_HAVE_ACOUNT_SIGN_IN = 'Tiene una cuenta? Entre'

@CONST_YOUR_EMAIL = 'Correo electrónico'
@CONST_PASSWORD = 'Contraseña'
@CONST_CONFIRM_PASSWORD = 'Confirmar contraseña'

#@CONST_SORRY_CANNOT_DELETE_FINAL_PUBLIC_LIST = 'Sorry, you cannot delete the final public list!'
@CONST_SORRY_CANNOT_DELETE_FINAL_PUBLIC_LIST = 'No es posible eliminar el último tema público!'
#@CONST_SURE_YOU_WANT_DELETE_LIST = 'Are you sure you want to delete the list #{list.name}?'
@CONST_SURE_YOU_WANT_DELETE_LIST = 'Está seguro de querer eliminar el tema: ' 
#@CONST_SIGN_CREATE_ACCOUNT_MAKE_PRIVATE_LISTS = 'Please sign in or create an account to make private lists.'
@CONST_SIGN_CREATE_ACCOUNT_MAKE_PRIVATE_LISTS = 'Entra o regístrate para poder crear temas.'
#@CONST_SORRY_CANNOT_MAKE_FINAL_PUBLIC_LIST_PRIVATE = 'Sorry, you cannot make the final public list private!'
@CONST_SORRY_CANNOT_MAKE_FINAL_PUBLIC_LIST_PRIVATE = 'No es posible cambiar el último tema público!'



