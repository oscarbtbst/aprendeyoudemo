Router.configure
  layoutTemplate: 'appBody'
  notFoundTemplate: 'appNotFound'
  loadingTemplate: 'appLoading'
  waitOn: ->
    [
      Meteor.subscribe('publicLists')
      Meteor.subscribe('privateLists')
    ]

dataReadyHold = null

if Meteor.isClient
  # Keep showing the launch screen on mobile devices until we have loaded
  # the app's data
  dataReadyHold = LaunchScreen.hold()
  # Show the loading screen on desktop
  Router.onBeforeAction 'loading', except: [
    'join'
    'signin'
  ]
  Router.onBeforeAction 'dataNotFound', except: [
    'join'
    'signin'
  ]

Router.map ->
  @route 'join'

  @route 'signin'

  @route 'listsShow',
    path: '/lists/:_id'
    onBeforeAction: ->
      @todosHandle = Meteor.subscribe('todos', @params._id)
      if @ready()
        # Handle for launch screen defined in app-body.js
        dataReadyHold.release()
      return
    data: ->
      Lists.findOne @params._id      
    action: ->
      @render()
      return      

  @route 'home',
    path: '/'
    action: ->
      Router.go 'listsShow', Lists.findOne()
      return
  return
