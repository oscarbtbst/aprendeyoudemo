@Lists = new (Mongo.Collection)('lists')
# Calculate a default name for a list in the form of 'List A'

Lists.defaultName = ->
  nextLetter = 'A'
  nextName = CONST_THEME + ' ' + nextLetter
  while Lists.findOne(name: nextName)
    # not going to be too smart here, can go past Z
    nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1)
    nextName = CONST_THEME + ' ' + nextLetter
  nextName

@Todos = new (Mongo.Collection)('todos')
