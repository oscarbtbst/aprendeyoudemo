Meteor.publish 'publicLists', ->
  Lists.find userId: $exists: false

Meteor.publish 'privateLists', ->
  if @userId
    return Lists.find(userId: @userId)
  else
    @ready()
  return

Meteor.publish 'todos', (listId) ->
  check listId, String
  Todos.find listId: listId
