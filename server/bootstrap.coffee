# if the database is empty on server start, create some sample data.
Meteor.startup ->

  if Meteor.users.find().count() == 0
    options =
      email: 'demo@hotmail.com'
      password: 'DeMo'
    Accounts.createUser(options)

  if Lists.find().count() == 0
    
    data = [
      {
        videoCount: 7
        name: 'Meteor.js'
        items: [
          'Documentación online:'
          'http://docs.meteor.com/#/basic/'
          '-'
          'Primer Videotutorial sobre Meteor en el que se explica la generalización de los llamados Frameworks MVC JavaScript. Se explica a que se debe su rápida expansión y los problemas que intentan solucionar. Pero también plantean nuevos retos a los que intenta responder el llamado javascript isomorfo.'
          'https://www.youtube.com/embed/F3j_CjyP75o'
          '-'
          'Segundo Videotutorial del Curso de Meteor.js, en el que después de la introducción genérica a las APIs MVC de Javascript y, en el caso de meteor, también isomorfa, vemos como instalarlo desde la consola y como empezar a trabajar con las aplicaciones de ejemplo que incluye meteor.js.'
          'https://www.youtube.com/embed/l4u1a2h1S08'
          '-'
          'Videotutorial nº 3 del Curso de Meteor.js, en el que empezamos por ver como acceder a los directorios y archivos que se han instalado con meteor. Seguimos trabajando con la aplicación de ejemplo instalada en el anterior videotutorial y empezamos a introducir cambios, lo que nos permitirá probar una de las características más interesantes de Meteor, la programación reactiva, como detecta inmediatamente cualquier cambio en nuestro código y lo transmite a la aplicación sin la necesidad de reiniciar el servidor y ni siquiera sin tener que actualizar el navegador.'
          'https://www.youtube.com/embed/uRI3Mv-dUj8'
          '-'
          'VideoTutorial nº 4 del Curso de meteor.js en el que empezamos a ver como persistir datos dinámicos. Vemos como añadir Colecciones en el archivo javascript de la aplicación y desde la consola del navegador, vemos como añadir Categorías e items y como acceder a ellos.'
          'https://www.youtube.com/embed/etFsyaPam2s'
          '-'
          'VideoTutorial nº 5 del Curso de Meteor.js en el que vemos como mostrar en nuestra página html las colecciones y categorías creadas en MongoDB. Usamos también Twitter Bootstrap para aplicarle estilos CSS en el diseño de la página.'
          'https://www.youtube.com/embed/6nwfEq_QQIQ'
          '-'
          'VideoTutorial nº 6 del Curso de Meteor.js en el que vemos el funcionamiento de la programación reactiva en acción. Creamos nuevas categorías, vemos como eliminarlas y como debe hacerse a través de la id. Terminamos resumiendo el funcionamiento de la programación y los contextos reactivos.'
          'https://www.youtube.com/embed/YpYuXAjWM7c'
          '-'
          'Videotutorial nº 7 del Curso de Meteor.js en el que vemos como aplica el patrón Model View ViewModel. ¿Qué tiene Meteor de especial? Las aplicaciones cliente/servidor; Los patrones MVC y MVC Anidado; El patrón de diseño MVVM; Creación de un Modelo en Meteor. Uso de templates como Modelo Vista.'
          'https://www.youtube.com/embed/Hf4VOWkwAGs'
          '-'
        ]
      }
      {
        videoCount: 2
        name: 'Git'
        items: [
          'Tutorial de Git en media hora:'
          'https://www.youtube.com/embed/QGKTdL7GG24'
          '-'
          'Aprende a usar Sourcetree de Atlassian:'
          'https://www.youtube.com/embed/QnkiKrruJiE'
          '-'
        ]
      }
      {
        videoCount: 5
        name: 'Scrum'
        items: [
          'Introducción al método de desarrollo ágil Scrum en menos de 10 minutos (en inglés):'
          'https://www.youtube.com/embed/XU0llRltyFM'
          '-'
          '1ª parte curso en profundidad sobre Scrum (en inglés):'
          'https://www.youtube.com/embed/D8vT7G0WATM'
          '-'
          '2ª parte curso Scrum - Reunión de depuración de pila de producto (en inglés):'
          'https://www.youtube.com/embed/b_WeHcZcx1w'
          '-'
          '3ª parte curso Scrum - Reunión de planificación de sprint (en inglés):'
          'https://www.youtube.com/embed/wPvG9NZNUa4'
          '-'
          'Metodologías ágiles. El proceso SCRUM:'
          'https://www.youtube.com/embed/p9MYRrQEOGI'
          '-'
        ]
      }
      {
        videoCount: 19
        name: 'Patrones de diseño'
        items: [
          'Introducción a los patrones de diseño. Utilizaremos el lenguaje de programación Java. Comenzamos con el patrón de diseño de creación llamado SINGLETON.'
          'https://www.youtube.com/embed/77Kw5P5Y73U'
          '-'
          'Seguimos con un ejemplo práctico del patrón de diseño Singleton. Un juego donde varias clases llaman a una clase tipo Singleton y no importa quien la esté manejando, ya que al final solo la clase singleton tiene toda la información del juego.'
          'https://www.youtube.com/embed/XoDXh2ttrsw'
          '-'
          'En este videotutorial comenzamos viendo el patron de diseño Fabrica (del grupo de creación). Pero como actualmente no puedo subir videos de mas de 15 min, entonces solo alcanzamos a ver un ejemplo que identifica las diferencias entre Interfaz y Clase abstracta.'
          'https://www.youtube.com/embed/WnOVW2Yrqss'
          '-'
          'En este videotutorial veremos un ejemplo práctico del patrón de diseño Factory y Abstract Factory.'
          'https://www.youtube.com/embed/i6EOpt5lg7U'
          '-'
          'Continuamos con el grupo de Creación. En esta ocasión vemos el patrón llamado Builder. El cual se encarga de permitir que el código cliente controle el proceso de construcción de los objetos, de manera que el mismo proceso de construcción pueda crear diferentes representaciones.'
          'https://www.youtube.com/embed/pHr01mluNVU'
          '-'
          'En esta ocasión veremos el patrón de diseño PROTOTYPE, con el cual se pueden clonar objetos ya existentes parar realizar cambios temporales. Este es el último videotutorial del grupo de CREACIÓN. No olviden suscribirse a mi canal realizar sus comentarios para mejorar los videotutoriales.'
          'https://www.youtube.com/embed/3qJdehZ2Y9U'
          '-'
          'Comenzamos el segundo bloque del curso. Empezamos con el grupo de Estructura, específicamente el patrón de diseño PROXY. Este nos sirve para conectarse a algún objeto remotamente sin tener que preocuparse de donde se encuentra el objeto original y también puede servir para controlar el acceso.'
          'https://www.youtube.com/embed/kyY-5--C4HU'
          '-'
          'Vemos el patrón de diseño ADAPTADOR, con el cual podemos adaptar objetos a un nuevo ambiente de acuerdo a las necesidades del programa.'
          'https://www.youtube.com/embed/bh_AjgYYd_w'
          '-'
          'En esta oportunidad veremos el patrón de diseño FACHADA (FACADE), el cual permite que el cliente se -desentienda- de la complejidad de un subsistema, permitiendo ejecutar acciones cortas para realizar acciones complejas.'
          'https://www.youtube.com/embed/mNfoTTUPy3I'
          '-'
          'Veremos el patron de diseño PUENTE (Bridge). El cual nos sirve para separar la implementación de la representación de un programa, permitiendo así ahorrar mucho código ya que se utilizan jerarquias independientes pero relacionadas.'
          'https://www.youtube.com/embed/S7rfpGzQaOg'
          '-'
          'En esta ocasión veremos el patrón de diseño COMPUESTO (COMPOSITE) el cual trabaja de forma recursiva para recorrer árboles y conocer propiedades de los nodos de dichos árboles.'
          'https://www.youtube.com/embed/s8PpqluRCTU'
          '-'
          'Vemos el patrón de diseño DECORADOR, el cual permite agregar funcionalidades adicionales a una clase base decorandola.'
          'https://www.youtube.com/embed/DIz4LmDX7ks'
          '-'
          'Comenzamos la tercera parte del curso. Veremos en esta tercera parte el grupo de patrones de COMPORTAMIENTO. Aquí veremos el patrón de diseño OBSERVADOR (OBSERVER). En este patrón, el sujeto notifica de los cambios de estado a los observadores registrados.'
          'https://www.youtube.com/embed/4NzbEDidVmU'
          '-'
          'En esta ocasión veremos la CADENA DE RESPONSABILIDAD, cuyo objetivo es: Evitar el acoplamiento de una solicitud desde el remitente a su receptor, dando a más de un objeto la oportunidad de manejar la petición.'
          'https://www.youtube.com/embed/Y3zwnT_5P5k'
          '-'
          'Veremos el patrón de diseño ESTADO el cual permite a un objeto cambiar su comportamiento dependiendo del estado en el que se encuentre. Separando el código del estado a clases independientes.'
          'https://www.youtube.com/embed/1fqmhGmAB4s'
          '-'
          'Veremos cómo el patrón de diseño estrategia nos permite crear algoritmos independientes que pueden ser utilizados en diferentes contextos. Veremos también las diferencias entre el patrón de diseño ESTADO y ESTRATEGIA.'
          'https://www.youtube.com/embed/sxNR8sAN9ow'
          '-'
          'Veremos el patrón de diseño MEMENTO, el cual simula un backup de un objeto en un momento dado.'
          'https://www.youtube.com/embed/YeisVHBt7l0'
          '-'
          'Veremos el patrón de diseño MÉTODO PLANTILLA, cuyo objetivo es compartir un algoritmo con todas las clases hijas.'
          'https://www.youtube.com/embed/q-VmXtW4HVU'
          '-'
          'Último patron de diseño del curso en el que veremos el patrón ITERADOR (ITERATOR), el cual nos permite recorrer los elementos de una estructura de datos sin necesidad de conocer su comportamiento.'
          'https://www.youtube.com/embed/Qzy-CwIO9zg'
          '-'
        ]
      }
      {
        videoCount: 3
        name: 'Jade & Coffescript'
        items: [
          'Introducción a plantillas Jade (HTML basado en sintaxis python):'
          'https://www.youtube.com/embed/jz-XcJBcjdQ'
          '-'
          'Introducción a CoffeeScript (JavaScript basado en sintaxis python)'
          'Parte 1, introducción e instalación:'
          'https://www.youtube.com/embed/e6UtcZzoSvc'
          '-'
          'Parte 2, Compilar CoffeeScript a JavaScript y primer programa:'
          'https://www.youtube.com/embed/wk-Zir-JWvk'
          '-'
        ]
      }
      {
        videoCount: 5
        name: 'AngularJS & Yeoman'
        items: [
          'AngularJS en 50 ejemplos parte 1 (en inglés):'
          'https://www.youtube.com/embed/TRrL5j3MIvo'
          '-'
          'AngularJS en 50 ejemplos parte 2 (en inglés):'
          'https://www.youtube.com/embed/6J08m1H2BME'
          '-'
          'AngularJS - Generar un Proyecto con YEOMAN:'
          'https://www.youtube.com/embed/eKSnXlIA0UQ'
          'Para utilizar AngularJS y YEOMAN con Node, consultar el siguiente link:'
          'https://github.com/yeoman/generator-angular'
          'Para utilizar AngularJS y YEOMAN con Meteor, consultar el siguiente link:'
          'https://www.npmjs.com/package/generator-angular-meteor'
          '-'
          'Introducción y exploración sobre AngularJS en 2015 (en inglés):'
          'https://www.youtube.com/embed/u2okVLCrqgE'
          '-'
          'Construyendo aplicaciónes móviles con Ionic-Angular-Cordova (en inglés)'
          'https://www.youtube.com/embed/bzmujVzTXXk'
          '-'
        ]
      }
      {
        videoCount: 9
        name: 'Ruby on Rails'
        items: [
          '1. Curso de Ruby on Rails 4 desde cero - Introducción:'
          'https://www.youtube.com/embed/LMD3P97gXa0'
          '-'
          '2. Curso de Ruby on Rails 4 desde cero - Primera aplicación:'
          'https://www.youtube.com/embed/d8-V2V9oMvI'
          '-'
          '3.- Curso Ruby on Rails 4 desde cero - ERB y Assets:'
          'https://www.youtube.com/embed/Anb-v8R0wtc'
          '-'
          '4.- Curso Ruby on Rails 4 desde 0 - Modelos:'
          'https://www.youtube.com/embed/qZjdzXjjfdo'
          '-'
          '5.- Curso Ruby on Rails 4 desde cero - Bases de Datos y Migraciones:'
          'https://www.youtube.com/embed/CnULQUq0FAU'
          '-'
          '6.- Curso Ruby on Rails 4 desde cero - Layouts:'
          'https://www.youtube.com/embed/I9hvSdEy238'
          '-'
          '7.- Curso Ruby on Rails 4 desde cero - Routes:'
          'https://www.youtube.com/embed/QK_c4nmmSOk'
          '-'
          '8.- Ruby on Rails 4 desde cero - Controladores:'
          'https://www.youtube.com/embed/-18fBp-hAWc'
          '-'
          'Ruby on Rails Tutorial: Desarrollando una aplicación de demostracíon (en inglés):'
          'https://www.youtube.com/embed/7Vj0u_q-iEw'
          '-'
          'Ruby on Rails - PDFs con Prawn - Ejemplo de pedido (en inglés):'
          'https://www.youtube.com/embed/e-A3zBeWDdE'
          '-'
          'Programación con Ruby (en inglés):'
          'https://www.youtube.com/embed/Dji9ALCgfpM'
          '-'
          'Tutorial Ruby on Rails - parte 1 (en inglés):'
          'https://www.youtube.com/embed/GY7Ps8fqGdc'
          '-'
          'Tutorial Ruby on Rails - parte 2 (en inglés):'
          'https://www.youtube.com/embed/eY1HCqe9MBc'
          '-'
          'AngularJS con Ruby on Rails (en inglés):'
          'https://www.youtube.com/embed/R96rw9vEtiQ'
          '-'
        ]
      }
      {
        videoCount: 8
        name: 'Wordpress'
        items: [
          'Tutorial Wordpress español 2014 desde cero:'
          'https://www.youtube.com/embed/videoseries?list=PLmIEoKACpcDy5XGw_v7RFxRQ04cvXCF40'
          '-'
          'Cómo crear una página usando IG PageBuilder (en inglés):'
          'https://www.youtube.com/embed/abkazb-NtkY'
          '-'
          'Tutorial del tema Virtue de Wordpress (en inglés):'
          'https://www.youtube.com/embed/6zp597oPqYU'
          '-'
          'Playlist tutorial de Wordpress para principiantes 2015 (en inglés):'
          'https://www.youtube.com/embed/3vVfEsi-R6M?list=PLfOXCtnURNbZjLUyU_Isp39VdAjqEctNw'
          '-'
          'Cómo construir Una web con WordPress - 2015 (en inglés):'
          'https://www.youtube.com/embed/XgNROudDNDs'
          '-'
          'Cómo crear una web eCommerce con WordPress - 2015 (en inglés):'
          'https://www.youtube.com/embed/yd-Luet5Jgo'
          '-'
          'Como usar plugin Wordpress Jetpack para mejorar tu sitio web (en inglés):'
          'https://www.youtube.com/embed/Ac2dsh5NPY4'
          'Explica los plugins más interesantes que contiene Jetpack, la mayoría de los cuales están relacionados con el hecho de compartir los posts publicados via redes sociales y email'
          '-'
          'Cómo añadir código PHP a tus posts, páginas y widgets WordPress (en inglés):'
          'https://www.youtube.com/embed/-CYe0q1K45I'
          '-'
        ]
      }
      {
        videoCount: 0
        name: 'Principios Meteor'
        items: [
          'Asimilación del ecosistema'
          'Simplicidad igual a productividad'
          'Reactividad completa en tiempo de diseño y ejecución'
          'Compensación de latencia'
          'Base de datos en todas partes (MongoDB)'
          'Un lenguaje (Javascript)'
          'Sólo datos por el canal de comunicación'
        ]
      }
    ]

    timestamp = (new Date).getTime()
    
    countlists = 1
    _.each data, (list) ->
      list_id = Lists.insert(
        name: list.name
        position: countlists
        incompleteCount: list.items.length
        videoCount: list.videoCount)
      countlists += 1
      
      counttodos = 1
      _.each list.items, (text) ->
        Todos.insert
          listId: list_id
          text: text
          createdAt: new Date(timestamp)
          position: counttodos
        timestamp += 1
        counttodos += 1
        # ensure unique timestamp.
        return
        
      return

  return
