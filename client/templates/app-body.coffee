Session.setDefault USER_MENU_KEY, false
Session.setDefault SHOW_CONNECTION_ISSUE_KEY, false
Session.setDefault MENU_KEY, false
Session.setDefault USER_MODE_READ, false
Session.setDefault CHANGING_USER_MODE, false


Meteor.startup ->
  # set up a swipe left / right handler
  $(document.body).touchwipe
    wipeLeft: ->
      Session.set MENU_KEY, false
      return
    wipeRight: ->
      Session.set MENU_KEY, true
      return
    preventDefaultEvents: false
  # Only show the connection error box if it has been 5 seconds since
  # the app started
  setTimeout (->
    # Launch screen handle created in lib/router.js
    @dataReadyHold.release()
    # Show the connection error box
    Session.set SHOW_CONNECTION_ISSUE_KEY, true
    return
  ), CONNECTION_ISSUE_TIMEOUT
  return


Template.appBody.rendered = ->
  @find('#content-container')._uihooks =
    insertElement: (node, next) ->
      $(node).hide().insertBefore(next).fadeIn ->
        @listFadeInHold.release()
        return
      return
    removeElement: (node) ->
      $(node).fadeOut ->
        $(this).remove()
        return
      return
  return


Template.appBody.helpers
  thisArray: ->
    [ this ]
  menuOpen: ->
    Session.get(MENU_KEY) and 'menu-open'
  cordova: ->
    Meteor.isCordova and 'cordova'


Template.appBody.events

  'click .js-menu': ->
    Session.set MENU_KEY, !Session.get(MENU_KEY)
    return

  'click .content-overlay': (event) ->
    Session.set MENU_KEY, false
    event.preventDefault()
    return

  'click #menu a': ->
    Session.set MENU_KEY, false
    return
