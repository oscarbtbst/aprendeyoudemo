ERRORS_KEY = 'signinErrors'

Template.signin.onCreated ->
  Session.set ERRORS_KEY, {}
  return

Template.signin.helpers
  errorMessages: ->
    _.values Session.get(ERRORS_KEY)
  errorClass: (key) ->
    Session.get(ERRORS_KEY)[key] and 'error'

  CONST_SIGN_IN: ->
    CONST_SIGN_IN
  CONST_SIGN_IN_DOT: ->
    CONST_SIGN_IN_DOT
  CONST_SING_IN_ALLOWS_MANAGE_PRIVATE_TOPICS: ->
    CONST_SING_IN_ALLOWS_MANAGE_PRIVATE_TOPICS
  CONST_NEED_ACOUNT_JOIN_NOW: ->
    CONST_NEED_ACOUNT_JOIN_NOW
  CONST_YOUR_EMAIL: ->
    CONST_YOUR_EMAIL
  CONST_PASSWORD: ->
    CONST_PASSWORD


Template.signin.events 'submit': (event, template) ->
  event.preventDefault()
  email = template.$('[name=email]').val()
  password = template.$('[name=password]').val()
  errors = {}
  if !email
    errors.email = 'Email is required'
  if !password
    errors.password = 'Password is required'
  Session.set ERRORS_KEY, errors
  if _.keys(errors).length
    return
  Meteor.loginWithPassword email, password, (error) ->
    if error
      return Session.set(ERRORS_KEY, 'none': error.reason)
    Session.set USER_MENU_KEY, false
    Router.go 'home'
    return
  return
