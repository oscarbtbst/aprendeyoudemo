ERRORS_KEY = 'joinErrors'

Template.join.onCreated ->
  Session.set ERRORS_KEY, {}
  return

Template.join.helpers
  errorMessages: ->
    _.values Session.get(ERRORS_KEY)
  errorClass: (key) ->
    Session.get(ERRORS_KEY)[key] and 'error'

  CONST_JOIN_DOT: ->
    CONST_JOIN_DOT
  CONST_JOIN_ALLOWS_MANAGE_PRIVATE_TOPICS: ->
    CONST_JOIN_ALLOWS_MANAGE_PRIVATE_TOPICS
  CONST_JOIN_NOW: ->
    CONST_JOIN_NOW
  CONST_HAVE_ACOUNT_SIGN_IN: ->
    CONST_HAVE_ACOUNT_SIGN_IN
  CONST_YOUR_EMAIL: ->
    CONST_YOUR_EMAIL
  CONST_PASSWORD: ->
    CONST_PASSWORD
  CONST_CONFIRM_PASSWORD: ->
    CONST_CONFIRM_PASSWORD


Template.join.events 'submit': (event, template) ->
  event.preventDefault()
  email = template.$('[name=email]').val()
  password = template.$('[name=password]').val()
  confirm = template.$('[name=confirm]').val()
  errors = {}
  if !email
    errors.email = 'Email required'
  if !password
    errors.password = 'Password required'
  if confirm != password
    errors.confirm = 'Please confirm your password'
  Session.set ERRORS_KEY, errors
  if _.keys(errors).length
    return

  Accounts.createUser {
    email: email
    password: password
  }, (error) ->
    if error
      return Session.set(ERRORS_KEY, 'none': error.reason)
    Session.set USER_MENU_KEY, false
    Router.go 'home'
    return
  
  return
