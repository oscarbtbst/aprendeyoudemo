
Template.appMenuBody.helpers
  emailLocalPart: ->
    email = Meteor.user().emails[0].address
    email.substring 0, email.indexOf('@')
  lists: ->
    Lists.find {}, sort: position: 1
  activeListClass: ->
    current = Router.current()
    if current.route.name == 'listsShow' and current.params._id == @_id
      return 'active'
    return
  connected: ->
    if Session.get(SHOW_CONNECTION_ISSUE_KEY)
      Meteor.status().connected
    else
      true
  userMenuOpen: ->
    Session.get USER_MENU_KEY
  userModeRead: ->
    #Session.set USER_MODE_READ, !Session.get(USER_MODE_READ)
    return Session.get USER_MODE_READ
  currentUserModeWrite: ->
    return Meteor.user() && !Session.get(USER_MODE_READ)
  
  CONST_SIGN_IN: ->
    CONST_SIGN_IN
  CONST_JOIN: ->
    CONST_JOIN
  CONST_LOGOUT: ->
    CONST_LOGOUT
  CONST_TRYING_CONNECT: ->
    CONST_TRYING_CONNECT
  CONST_SEEMS_CONNECTION_ISSUE: ->
    CONST_SEEMS_CONNECTION_ISSUE
  CONST_NEW_TOPIC: ->
    CONST_NEW_TOPIC
  CONST_CHANGE_TO_EDIT_MODE: ->
    CONST_CHANGE_TO_EDIT_MODE
  CONST_CHANGE_TO_READ_MODE: ->
    CONST_CHANGE_TO_READ_MODE
  CONST_MOVE_TOPIC_DOWN: ->
    CONST_MOVE_TOPIC_DOWN


Template.appMenuBody.events
  'mousedown .js-user-mode': (event) ->
    # stop the menu from closing
    return
  'click .js-user-mode': (event) ->
    if !Session.get CHANGING_USER_MODE
      Session.set CHANGING_USER_MODE, true
    else
      Session.set CHANGING_USER_MODE, false
      Session.set USER_MODE_READ, !Session.get(USER_MODE_READ)
      Session.set USER_MODE_READ_LISTS_SHOW, !Session.get(USER_MODE_READ_LISTS_SHOW)
    # stop the menu from closing
    event.stopImmediatePropagation()
    return
  'click .js-user-menu': (event) ->
    Session.set USER_MENU_KEY, !Session.get(USER_MENU_KEY)
    # stop the menu from closing
    event.stopImmediatePropagation()
    return

  'click .js-logout': ->
    Meteor.logout()
    # if we are on a private list, we'll need to go to a public one
    current = Router.current()
    if current.route.name == 'listsShow' and current.data().userId
      Router.go 'listsShow', Lists.findOne(userId: $exists: false)
    return

  'click .js-logout': ->
    Meteor.logout()
    # if we are on a private list, we'll need to go to a public one
    current = Router.current()
    if current.route.name == 'listsShow' and current.data().userId
      Router.go 'listsShow', Lists.findOne(userId: $exists: false)
    return

  'click .js-new-list': ->
    #Reorder lists
    listscount = 1
    lists = Lists.find {}, sort: position: 1
    lists.forEach (list) ->
      Lists.update list._id, $set: position: listscount
      listscount += 1
      return
    Meteor._debug "add -> reorder lists #{listscount}"

    list = 
      name: Lists.defaultName()
      incompleteCount: 0
      videoCount: 0
      userId: Meteor.userId()
      position: listscount

    list._id = Lists.insert(list)
    Router.go 'listsShow', list
    return

  'mousedown .js-down-list': ->
    list = Lists.findOne _id: @_id
    list2 = Lists.findOne {position: list.position+1}
    if list2
      Meteor._debug 'list.switchpositiondown #{list.position}'
      Lists.update list._id, $set: position: list2.position
      Lists.update list2._id, $set: position: list.position
    return
