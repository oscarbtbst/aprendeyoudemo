EDITING_KEY = 'EDITING_TODO_ID'

Template.todosItem.helpers
  checkedClass: ->
    @checked and 'checked'
  editingClass: ->
    Session.equals(EDITING_KEY, @_id) and 'editing'
  isYoutube: ->
    Meteor._debug @text
    return @text.indexOf("youtube.com/embed") > -1
  isURL: ->
    return @text.indexOf("http://") > -1 or @text.indexOf("https://") > -1 
  isSeparator: ->
    return @text == '-'
  currentUserModeWrite: ->
    #@find('textarea').elastic()
    return Meteor.user() && !Session.get(USER_MODE_READ)

  CONST_ELEMENT_CONTENT: ->
    CONST_ELEMENT_CONTENT


Template.todosItem.events

  'change [type=checkbox]': (event) ->
    #$('textarea').elastic()

    checked = $(event.target).is(':checked')
    Todos.update @_id, $set: checked: checked
    Lists.update @listId, $inc: incompleteCount: if checked then -1 else 1
    return

#  'focus input[type=text]': (event) ->
#    Session.set EDITING_KEY, @_id
#    return

#  'blur input[type=text]': (event) ->
#    if Session.equals(EDITING_KEY, @_id)
#      Session.set EDITING_KEY, null
#    return

#  'keydown input[type=text]': (event) ->
#    # ESC or ENTER
#    if event.which == 27 or event.which == 13
#      event.preventDefault()
#      event.target.blur()
#    return

#  'keyup input[type=text]': _.throttle(((event) ->
#    Todos.update @_id, $set: text: event.target.value
#    return
#  ), 300)

  'change textarea': (event) ->
    #@find('#textarea').elastic()
    event.target.elastic
    Todos.update @_id, $set: text: event.target.value

  'mousedown .js-delete-item, click .js-delete-item': ->
    Todos.remove @_id

    #Reorder todos list
    todoscount = 1
    videocnt = 0
    todoslist = Todos.find {listId: @listId}, sort: position: 1
    todoslist.forEach (todo) ->
      Todos.update todo._id, $set: position: todoscount
      todoscount += 1
      if todo.text.indexOf("youtube.com/embed") > -1
         videocnt += 1
      return
    Meteor._debug "remove -> reorder todos list #{todoscount}"

    Lists.update @listId, $set: videoCount: videocnt
    if !@checked
      Lists.update @listId, $inc: incompleteCount: -1
    return

  'mousedown .js-down-item, click .js-down-item:': ->
    todo = Todos.findOne _id: @_id
    todo2 = Todos.findOne {listId: todo.listId, position: todo.position+1}
    if todo2
      Meteor._debug 'todo.switchpositionup #{todo.position}'
      Todos.update todo._id, $set: position: todo2.position
      Todos.update todo2._id, $set: position: todo.position
    return


  'mousedown .js-up-item, click .js-up-item:': ->
    todo = Todos.findOne _id: @_id
    todo2 = Todos.findOne {listId: todo.listId, position: todo.position-1}
    if todo2
      Meteor._debug 'todo.switchpositiondown #{todo.position}'
      Todos.update todo._id, $set: position: todo2.position
      Todos.update todo2._id, $set: position: todo.position
    return
