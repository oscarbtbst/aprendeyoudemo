EDITING_KEY = 'editingList'
Session.setDefault EDITING_KEY, false

# Track if this is the first time the list template is rendered
firstRender = true
listRenderHold = LaunchScreen.hold()
listFadeInHold = null

Template.listsShow.onRendered ->
  #$('textarea').elastic()
  
  if firstRender
    # Released in app-body.js
    listFadeInHold = LaunchScreen.hold()
    # Handle for launch screen defined in app-body.js
    listRenderHold.release()
    firstRender = false
  @find('.js-title-nav')._uihooks =
    insertElement: (node, next) ->
      $(node).hide().insertBefore(next).fadeIn()
      return
    removeElement: (node) ->
      $(node).fadeOut ->
        @remove()
        return
      return
  return

Template.listsShow.helpers
  editing: ->
    Session.get EDITING_KEY
  todosReady: ->
    Router.current().todosHandle.ready()
  todos: (listId) ->
    Todos.find { listId: listId }, sort: position: 1
  listsshowwriteread: ->
    if Meteor.user() && !Session.get(USER_MODE_READ_LISTS_SHOW)
      'lists-show-write'
    else
      'lists-show-read'
  currentUserModeWrite: ->
    return Meteor.user() && !Session.get(USER_MODE_READ)
  changingUserMode: ->
    if Session.get(CHANGING_USER_MODE)
      true
    else
      false

  CONST_SELECT_AN_ACTION: ->
    CONST_SELECT_AN_ACTION
  CONST_MAKE_TOPIC_PUBLIC: ->
    CONST_MAKE_TOPIC_PUBLIC
  CONST_MAKE_TOPIC_PRIVATE: ->
    CONST_MAKE_TOPIC_PRIVATE
  CONST_SHARE_TOPIC_REGISTERED_USERS: ->
    CONST_SHARE_TOPIC_REGISTERED_USERS
  CONST_DELETE: ->
    CONST_DELETE
  CONST_CANCEL: ->
    CONST_CANCEL
  CONST_SHOW_MENU: ->
    CONST_SHOW_MENU
  CONST_THERE_IS_NO_ELEMENTS_HERE: ->
    CONST_THERE_IS_NO_ELEMENTS_HERE
  CONST_ADD_NEW_ELEMENTS_WITH_FIELD_ABOVE: ->
    CONST_ADD_NEW_ELEMENTS_WITH_FIELD_ABOVE
  CONST_LOADING_ELEMENTS: ->
    CONST_LOADING_ELEMENTS
  CONST_WRITE_NEW_ELEMENT_HERE: ->
    CONST_WRITE_NEW_ELEMENT_HERE


editList = (list, template) ->
  if Meteor.user()
    Session.set EDITING_KEY, true
    # force the template to redraw based on the reactive change
    Tracker.flush()
    template.$('.js-edit-form input[type=text]').focus()
  return

saveList = (list, template) ->
  Session.set EDITING_KEY, false
  Lists.update list._id, $set: name: template.$('[name=name]').val()
  return

deleteList = (list) ->
  # ensure the last public list cannot be deleted.
  if !list.userId and Lists.find(userId: $exists: false).count() == 1
    return alert(CONST_SORRY_CANNOT_DELETE_FINAL_PUBLIC_LIST)
  message = CONST_SURE_YOU_WANT_DELETE_LIST + list.name + '?'
  if confirm(message)
    # we must remove each item individually from the client
    Todos.find(listId: list._id).forEach (todo) ->
      Todos.remove todo._id
      return
    Lists.remove list._id
    Router.go 'home'
    true
  else
    false

toggleListPrivacy = (list) ->
  if !Meteor.user()
    return alert(CONST_SIGN_CREATE_ACCOUNT_MAKE_PRIVATE_LISTS)
  if list.userId
    Lists.update list._id, $unset: userId: true
  else
    # ensure the last public list cannot be made private
    if Lists.find(userId: $exists: false).count() == 1
      return alert(CONST_SORRY_CANNOT_MAKE_FINAL_PUBLIC_LIST_PRIVATE)
    Lists.update list._id, $set: userId: Meteor.userId()
  return


Template.listsShow.events

  'click .js-cancel': ->
    Session.set EDITING_KEY, false
    return

  'keydown input[type=text]': (event) ->
    # ESC
    if 27 == event.which
      event.preventDefault()
      $(event.target).blur()
    return

  'blur input[type=text]': (event, template) ->
    # if we are still editing (we haven't just clicked the cancel button)
    if Session.get(EDITING_KEY)
      saveList this, template
    return

  'submit .js-edit-form': (event, template) ->
    event.preventDefault()
    saveList this, template
    return

  'mousedown .js-cancel, click .js-cancel': (event) ->
    event.preventDefault()
    Session.set EDITING_KEY, false
    return

  'change .list-edit': (event, template) ->
    if $(event.target).val() == 'edit'
      editList this, template
    else if $(event.target).val() == 'delete'
      deleteList this, template
    else
      toggleListPrivacy this, template
    event.target.selectedIndex = 0
    return

  'click .js-edit-list': (event, template) ->
    editList this, template
    return

  'click .js-toggle-list-privacy': (event, template) ->
    toggleListPrivacy this, template
    return

  'click .js-delete-list': (event, template) ->
    deleteList this, template
    return

  'click .js-todo-add': (event, template) ->
    template.$('.js-todo-new input').focus()
    return

  'submit .js-todo-new': (event) ->
    event.preventDefault()
    $input = $(event.target).find('[type=text]')
    if !$input.val()
      return

    #Reorder todos list
    todoscount = 1
    todoslist = Todos.find {listId: @_id}, sort: position: 1
    todoslist.forEach (todo) ->
      Todos.update todo._id, $set: position: todoscount
      todoscount += 1
      return
    Meteor._debug "add -> reorder todos list #{todoscount}"
    
    Todos.insert
      listId: @_id
      text: $input.val()
      checked: false
      createdAt: new Date
      position: todoscount

    Lists.update @_id, $inc: incompleteCount: 1

    $input.val ''
    return

  'submit .js-todo-new-top': (event) ->
    event.preventDefault()
    $input = $(event.target).find('[type=text]')
    if !$input.val()
      return

    #Reorder todos list
    todoscount = 2
    videocnt = 0
    todoslist = Todos.find {listId: @_id}, sort: position: 1
    todoslist.forEach (todo) ->
      Todos.update todo._id, $set: position: todoscount
      todoscount += 1

      if todo.text.indexOf("youtube.com/embed") > -1
         videocnt += 1
      return
    Meteor._debug "add -> reorder todos list #{todoscount}"
    
    if $input.val().indexOf("youtube.com/embed") > -1
      videocnt += 1

    Todos.insert
      listId: @_id
      text: $input.val()
      checked: false
      createdAt: new Date
      position: 1

    Lists.update @_id, $set: videoCount: videocnt
    Lists.update @_id, $inc: incompleteCount: 1

    $input.val ''
    return
